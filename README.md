# Lox Distributor

The Lox distributor receives resources from [rdsys](https://gitlab.torproject.org/tpo/anti-censorship/rdsys) and writes them to [Lox
BridgeLines](https://git-crysp.uwaterloo.ca/iang/lox/src/master/src/bridge_table.rs#L42). Concurrently, it receives and responds to requests from [Lox clients](https://gitlab.torproject.org/tpo/anti-censorship/lox/lox-wasm). 

## Configure rdsys stream

A test `config.json` is included for testing on a local instance of rdsys. This
can be edited to correspond to the desired types of resources and endpoints.

## Test Run

For testing purposes, you will need a running instance of rdsys as well as a running Lox client.

### Run rdsys locally

First clone rdsys from [here](https://gitlab.torproject.org/tpo/anti-censorship/rdsys) then change into the backend directory:

```
cd rdsys/cmd/backend
```

Finally run rdsys:

```
 ./backend --config config.json
```

### Run Lox Distributor locally

Simply run `cargo run -- config.json` :)

### Run a Lox client locally

First clone lox-wasm from [here](https://gitlab.torproject.org/tpo/anti-censorship/lox/lox-wasm). Follow the instructions in the [README](https://gitlab.torproject.org/tpo/anti-censorship/lox/lox-wasm/-/blob/main/README.md) to build and test the Lox client.
